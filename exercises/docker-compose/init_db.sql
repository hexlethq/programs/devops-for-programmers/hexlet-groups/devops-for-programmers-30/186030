CREATE TABLE users (
   id integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
   user_name varchar(255) UNIQUE NOT NULL,
   password varchar(255) NOT NULL,
   created_at timestamp DEFAULT NOW()
);
INSERT INTO users (user_name, password) VALUES ('user1', 'pass1');
