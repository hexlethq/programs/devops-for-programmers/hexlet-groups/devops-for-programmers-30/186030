const express = require('express');
const { Pool } = require('pg');

const connectDb = () => {
  const pool = new Pool({
    host: 'db',
    user: 'postgres',
    password: 'postgres',
    max: 20,
  });
  return pool;
};

const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hello World!')
});

app.get('/db', async (req, res) => {
  const pool = connectDb();
  const client = await pool.connect();

  try {
    const result = await client.query(
      `SELECT * FROM users;`,
    );
    res.send(result.rows);

  } catch (err) {
    console.log(err);
    res.status(500).send();
  } finally {
    client.release();
  }

});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});
