terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "ssh1" {
  // Имя под которым ключ сохранён в DO
  // https://cloud.digitalocean.com/account/security
  name = "devopsHome22"
}

resource "digitalocean_droplet" "web1" {
  image  = "docker-20-04"
  name   = "web-terraform-homework-01"
  region = "ams3"
  size   = "s-1vcpu-1gb"

  ssh_keys = [
    data.digitalocean_ssh_key.ssh1.id
  ]
}

resource "digitalocean_droplet" "web2" {
  image  = "docker-20-04"
  name   = "web-terraform-homework-02"
  region = "ams3"
  size   = "s-1vcpu-1gb"

  ssh_keys = [
    data.digitalocean_ssh_key.ssh1.id
  ]
}

resource "digitalocean_loadbalancer" "balancer1" {
  name   = "balancer-terraform-homework-01"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 5000
    protocol = "http"
    path     = "/"
  }

  droplet_ids = [digitalocean_droplet.web1.id, digitalocean_droplet.web2.id]
}

output "droplets_ips" {
  value = [
    digitalocean_droplet.web1.ipv4_address,
    digitalocean_droplet.web2.ipv4_address
  ]
}
