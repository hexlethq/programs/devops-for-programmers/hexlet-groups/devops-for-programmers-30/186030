# Мониторинг

В этой домашней работе мы подключим Datadog к нашим серверам Digital Ocean и настроим монитор, который будет проверять доступность нашего приложения, и, в случае отклонения от нормы, посылать уведомление об этом по электронной почте.

## Ссылки

* [Datadog](https://www.datadoghq.com/)
* [HTTP Check](https://docs.datadoghq.com/integrations/http_check/) - монитор состояния приложения, который мы будем устанавливает. Опрашивает определенный адрес каждые N секунд
* [Alerting](https://docs.datadoghq.com/monitors/) - Документация Datadog по алертам и мониторам
* [https://galaxy.ansible.com/DataDog/datadog](https://galaxy.ansible.com/DataDog/datadog) - роль Ansible Galaxy для установки Datadog
* [Installing roles and collections from the same requirements.yml file](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#installing-roles-and-collections-from-the-same-requirements-yml-file) - документация Ansible по работе с зависимостями

## Задачи

* Зарегистрируйтесь на [datadoghq.com](https://app.datadoghq.com/signup)
* На шаге *3. Agent Setup*. Выберите Ansible в качестве способа установки.
* Добавьте в роль `datadog.datadog` в *requirements.yml*.
* Добавьте роль `datadog.datadog` в плейбук. Добавьте переменные из шага в плейбук. Зашифруйте ключ апи с помощью Ansible Vault.
* В качестве проверки состояния приложения будет использовать [http_check](https://docs.datadoghq.com/integrations/http_check/) Он добавляется с помощью переменных в плейбуке. Агент будет проверять, что наше приложение запущено на 5000 порту:

    ```yaml
    http_check:
        init_config:
        instances:
            - name: Application health check status
                url: http://localhost:5000
                timeout: 5
                method: GET
    ```

* Если агент настроен правильно, то через 2-3 минуты информация будет отправлена в Datadog. Создайте новый монитор в разделе Network. Выберите созданный *http_check*, далее вам необходимо выбрать все хосты, сколько проверок требуется для оповещения и восстановления.
* В сообщении оповещения сделайте заголовок *Hexlet HTTP Alert! <host_name>*, где host_name - это переменная, которая будет подставляться из переменных Datadog.
* Добавьте себя в оповещение. Проверьте, что оповещение настроено корректно, отправьте тестовое сообщение с помощью соответствующей кнопки.
* Зайдите на один из серверов и остановите контейнер приложения. Если все было настроено правильно, то придет оповещение от Datadog о недоступности сервиса.
* В файле *solution* добавьте ссылку на задеплоенное приложение: http://<адрес>

После выполнения домашней работы приложите скриншоты в директории *screenshots*:

1. Запущенный монитор HTTP

![](assets/monitor.png)

2. Сработавший алерт монитора

![](assets/alert.png)

3. Тело письма оповещения

![](assets/alert-notification.png)

## Подсказки

* Чтобы агент Datadog запускался после приложения, используйте [ansible.builtin.include_role](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/include_role_module.html)

## Дополнительное задание

Настройте дашборд и выведите текущие метрики сервера.
